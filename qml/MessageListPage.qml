import QtQuick 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import "Functions.js" as Functions

Page {
    id: messageListPage

    property var messages: []
    property bool loading: false
    property bool refreshing: false
    property string error: ''

    function refreshMessages(useRefreshing) {
        if (settings.url && settings.clientToken) {
            if (useRefreshing) {
                refreshing = true;
            }
            else {
                loading = true;
            }

            client.getAllMessages(function(err, data) {
                if (useRefreshing) {
                    refreshing = false;
                }
                else {
                    loading = false;
                }

                if (err) {
                    error = err;
                    messages = [];
                }
                else {
                    error = '';
                    messages = data.messages;
                }

                messagesModel.clear();
                messages.forEach(function(message) {
                    message.dataId = message.id;
                    message.messageTitle = message.title;
                    delete message.id;
                    delete message.title;

                    messagesModel.append(message);
                });
            });
        }
    }

    Component.onCompleted: refreshMessages()

    Connections {
        target: pageStack
        onCurrentPageChanged: {
            if (pageStack.currentPage == messageListPage) {
                refreshMessages();
            }
        }
    }

    ListModel {
        id: messagesModel
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Gotify')

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                },

                Action {
                    text: i18n.tr('Settings')
                    iconName: 'settings'

                    onTriggered: pageStack.push(Qt.resolvedUrl('SettingsPage.qml'))
                },

                Action {
                    text: i18n.tr('Send Message')
                    iconName: 'send'

                    onTriggered: pageStack.push(Qt.resolvedUrl('MessagePage.qml'));
                }
            ]
        }
    }

    ActivityIndicator {
        visible: loading
        running: loading
        anchors.centerIn: parent
    }

    Label {
        anchors.centerIn: parent
        width: parent.width
        visible: error

        text: error
        color: UbuntuColors.red

        wrapMode: Label.WordWrap
        horizontalAlignment: Label.AlignHCenter
    }

    Label {
        anchors.centerIn: parent
        visible: !settings.url || !settings.clientToken

        text: i18n.tr('Configure server and token in the settings page')
        color: UbuntuColors.red

        horizontalAlignment: Label.AlignHCenter
    }

    Label {
        anchors.centerIn: parent
        visible: messagesModel.count === 0 && settings.url && settings.clientToken && !error && !loading

        text: i18n.tr('No messages')

        horizontalAlignment: Label.AlignHCenter
    }

    Flickable {
        visible: !loading

        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true

        ListView {
            anchors.fill: parent
            model: messagesModel

            PullToRefresh {
                refreshing: messageListPage.refreshing
                onRefresh: refreshMessages(true);
            }

            delegate: ListItem {
                id: listItem
                height: layout.height

                property string url: {
                    var match = message.match(/(https?:\/\/[^\s]+)/g);
                    if (match && match.length > 0) {
                        return match[0];
                    }

                    return '';
                }

                leadingActions: ListItemActions {
                    actions: Action {
                        iconName: 'delete'
                        text: i18n.tr('Delete Message')

                        onTriggered: {
                            client.deleteMessage(dataId, function() {
                                messagesModel.remove(index);
                            });
                        }
                    }
                }

                ListItemLayout {
                    id: layout
                    title.text: messageTitle
                    subtitle.text: message

                    ProgressionSlot {
                        visible: url
                    }

                    Item {
                        id: timeLabelSlot
                        width: timeLabel.width
                        height: parent.height
                        SlotsLayout.overrideVerticalPositioning: true
                        Label {
                            id: timeLabel
                            text: Functions.getMessageTime(date)
                            color: Theme.palette.normal.backgroundTertiaryText
                            fontSize: "small"
                            y: layout.mainSlot.y + layout.title.y
                            + layout.title.baselineOffset - baselineOffset
                        }
                    }
                }

                onClicked: {
                    if (url) {
                        Qt.openUrlExternally(url);
                    }
                }
            }
        }
    }

    Connections {
        target: pushClient
        onNotificationsChanged: refreshMessages()
    }
}
