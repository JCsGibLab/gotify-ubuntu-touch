// Convert RFC3339 datetime ("2006-01-02T15:04:05Z07:00") to qml date localized representation
function getMessageTime ( dateString ) {
    var date = new Date(Date.parse(dateString))
    var now = new Date ()
    var locale = Qt.locale()
    var fullTimeString = date.toLocaleTimeString(locale, Locale.ShortFormat)

    if ( date.getDate()  === now.getDate()  &&
    date.getMonth() === now.getMonth() &&
    date.getFullYear() === now.getFullYear() ) {
        return fullTimeString
    }

    return date.toLocaleString(locale, Locale.ShortFormat)
}
