import QtQuick 2.7
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import Ubuntu.PushNotifications 0.1
import Ubuntu.Content 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'gotify.bhdouglass'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property QtObject colors: QtObject {
        readonly property color divider: UbuntuColors.porcelain
        readonly property color text: UbuntuColors.porcelain
        readonly property color background: '#3f51b5'
    }

    Settings {
        id: settings
        property string url
        property string appToken
        property string clientToken
    }

    Client {
        id: client
    }

    PushClient {
        id: pushClient
        appId: 'gotify.bhdouglass_gotify'
        onTokenChanged: console.log('push client token:', token)
        onNotificationsChanged: console.log('push client notified')
    }

    function sharedLink(url) {
        // Go back to the first page on the stack
        while (pageStack.depth > 1) {
            pageStack.pop();
        }
        pageStack.push(Qt.resolvedUrl('MessagePage.qml'));

        pageStack.currentPage.message = url;
        pageStack.currentPage.title = i18n.tr('Link');
    }

    Connections {
        target: ContentHub

        onImportRequested: sharedLink(transfer.items[0].url);
        onShareRequested: sharedLink(transfer.items[0].url);
    }

    PageStack {
        id: pageStack

        Component.onCompleted: push(Qt.resolvedUrl('MessageListPage.qml'))
    }
}
